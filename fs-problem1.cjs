

const fs=require('fs');

function deleteFiles(path,n){
    return new Promise((resolve,reject) => {
        count=0;
        for(let idx=0; idx<n; idx++){
            let filePath=path+'/'+idx+'.cjs';
            fs.unlink(filePath, (err)=>{
            if(err){
                reject(err);
            }else{
                console.log("File: ",filePath,"was deleted.");
                count++;
                if(count==n-1){
                    resolve("All deleted successfully");
                }
            }
            });
        }
    });
}

function createFiles(path,n){
    return new Promise((resolve,reject) => {
        count=0;
        for(let idx=0; idx<n; idx++){
            let filePath=path+'/'+idx+'.cjs';
            fs.writeFile(filePath, "", (err)=>{
            if(err){
                console.log(count,"count");
                reject(err);
            }else{
                console.log("File: ",filePath,"was created.");
                count++;
                if(count==n-1){
                    console.log(count,"count");
                    resolve("All created successfully");
                }
            }
            });
        }
    });
}

function createDir(path){
    return new Promise((resolve,reject) => {
        fs.mkdir(path, (err) => {
            if (err){
                reject(err);
            }else{
                resolve("Directory created");
            }
        })
    })
}

function deleteDir(path){
    return new Promise((resolve,reject) => {
        fs.rmdir(path, (err) => {
            if (err){
                reject(err);
            }else{
                resolve("Directory deleted");
            }
        })
    })
}


function fsProblem1(path,n){
    createDir(path).then((msg) =>{
        console.log("createDir resolved",msg);
        return createFiles(path,n);
    }).then((msg) => {
        console.log("createFiles resolved",msg);
        return deleteFiles(path,n);
    }).then((msg) => {
        console.log("deleteFiles resolved",msg);
        return deleteDir(path);
    }).then((msg) => {
        console.log("deleteDir resolved",msg);
    }).catch((e) => {
        console.log("catch",e.message);
    });
}

module.exports=fsProblem1;
