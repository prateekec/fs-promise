const fs=require('fs');

function readInUpperLower(path,flag){
    return new Promise((resolve,reject) => {
    fs.readFile(path,'utf-8',(err,data) => {
        if(err){
            reject(err);
        }else{
            if(flag=="Upper"){
                resolve(data.toUpperCase());
            }else if(flag=="Lower"){
                resolve(data.toLowerCase());
            }else{
                resolve(data);
            }
        }
    })
});
}

function writeInFile(path,data,flag=true){
    return new Promise((resolve,reject) => {
        if(flag){
            fs.writeFile(path,data,(err) => {
                if(err){
                    reject(err);
                }else{
                    resolve("File has been created");
                }
            })
        }else{
            fs.appendFile(path,data+'\n',(err) => {
                if(err){
                    reject(err);
                }else{
                    resolve("Filename appended");
                }
            })
        }
    })
}

function splitInSentences(data){
    return new Promise ((resolve, reject) => {
        const sentencePattern = /(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?|\!)\s/g;
        data=data.split(sentencePattern);
        data=JSON.stringify(data);
        resolve(data);
    })
}

function deleteFiles(path,data){
    return new Promise((resolve,reject) => {
        data=data.splice(0,data.length-1);
        let count=data.length;
        for(let idx=0; idx<data.length; idx++){
            let file=data[idx];
            let filePath =path+'/'+file;
            console.log(filePath);
            fs.unlink(filePath,(err)=>{
                if(err){
                    reject(err);
                }else{
                    count--;
                    if(count==0){
                        resolve("Deleted all files");
                    }
                }
            })
        }
    })
}

function fsProblem2(path,file){
    let filePath =path+'/'+file;
    readInUpperLower(filePath,'Upper').then((data) =>{
        let filePath=path+'/'+"upper.txt";
        return writeInFile(filePath,data);
    }).then((msg) =>{
        console.log(msg);
        let filePath=path+'/'+"filenames.txt";
        let data="upper.txt";
        return writeInFile(filePath,data,false);
    }).then((msg) => {
        console.log(msg);
        let filePath=path+'/'+"upper.txt";
        return readInUpperLower(filePath,'Lower');
    }).then((data) => {
        return splitInSentences(data);
    }).then((data) => {
        let filePath= path+'/'+'lower+split.txt';
        return writeInFile(filePath,data);
    }).then((msg) => {
        console.log(msg);
        let filePath=path+'/'+'filenames.txt';
        let data='lower+split.txt';
        return writeInFile(filePath,data,false);
    }).then((msg) =>{
        console.log(msg);
        let filePath=path+'/'+'lower+split.txt';
        return readInUpperLower(filePath);
    }).then((data) => {
        data=JSON.parse(data);
        data=data.sort();
        data=JSON.stringify(data);
        let filePath =path+'/'+'sorted.txt';
        return writeInFile(filePath,data);       
    }).then((msg)=>{
        console.log(msg);
        let filePath=path+'/'+'filenames.txt'
        let data='sorted.txt';
        return writeInFile(filePath,data,false);
    }).then((msg) => {
        console.log(msg);
        let filePath=path+'/'+'filenames.txt';
        return readInUpperLower(filePath);
    }).then((data) => {
        data=JSON.stringify(data);
        data=JSON.parse(data);
        let files=data.split('\n');
        
        return deleteFiles(path,files);
    }).then((msg) => {
        console.log(msg);
    })
    .catch((err) =>{
        console.log(err);
    });

}

module.exports=fsProblem2;